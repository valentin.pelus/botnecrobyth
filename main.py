import discord
import os
import locale
locale.setlocale(locale.LC_TIME, '') #permet de mettre les jours en français avec !time (anglais par défaut)

from discord.ext import commands
from dotenv import load_dotenv
from datetime import datetime

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
PREFIX = os.getenv('DISCORD_PREFIX')

client = discord.Client()

bot = commands.Bot(command_prefix = PREFIX)


@bot.event
async def on_ready() :
	print("NecroBot prêt !")


@bot.command()
async def slt(ctx): #à chaque fois qu'il y a "async" il faut mettre "await ctx.send()" à la fin du code pour envoyer un message
	await ctx.send("Salut !")


@bot.command()
async def serv(ctx):
	serveur = ctx.guild
	nbSalonTxt = len(serveur.text_channels)
	nbSalonVoc = len(serveur.voice_channels)
	nbMembre = serveur.member_count
	nomServeur = serveur.name
	#descriServeur = serveur.description  #Si je veux afficher la description. Ici c'est inutile.
	message = "Le serveur **{0}** contient {1} membre(s). \nIl y a ici {2} salon(s) textuel(s) et {3} salon(s) vocal/vocaux.".format(nomServeur,nbMembre,nbSalonTxt,nbSalonVoc)
	str(message)
	await ctx.send(message)


@bot.command()
async def time(ctx):
	message = "Nous sommes le {0}".format(datetime.now().strftime('**%d-%m-%Y**' " et il est " '**%H:%M:%S**'))
	str(message)
	await ctx.send(message)


@bot.command() #la commande qui n'affiche pas la date dans le bon format
async def anniv(ctx,arg):
	d = datetime.strptime(arg, "%d-%m-%Y").strftime("%d-%m-%Y")
	await ctx.send(d)

bot.run(TOKEN)